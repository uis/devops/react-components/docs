/**
 * A simple node script that outputs a list of npm packages for a maintainer.
 */ 
var NpmApi = require('npm-api');

const api = new NpmApi();

if (process.argv.length !== 3) {
  console.error('Usage: node components_list.js {maintainer username}');
  process.exit(1)
}

api.maintainer(process.argv[2]).repos().then(function(repos) {
  console.log(repos.map(repo => repo.split('/')[1]).join(' '));
})
