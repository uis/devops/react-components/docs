# React Component Documentation Build

This is a deployment project that builds the  [`styleguidist`](https://react-styleguidist.js.org/)
documentation for all 
[DevOps common React components](https://gitlab.developers.cam.ac.uk/groups/uis/devops/react-components).
When one of these components is updated
[this build is triggered](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html#triggering-a-downstream-pipeline-using-a-bridge-job)
and the following tasks are performed by the pipeline:

- retrieve a list of the components to document
- creates merged set of the components' dependencies
- creates a merge of the components' source from GitLab
- creates the documentation with `styleguidist`
- pushes the updated documentation to [GitHub Pages](http://uisautomation.github.io/)

The following
[CI/CD Variables](https://gitlab.developers.cam.ac.uk/uis/devops/react-components/docs/-/settings/ci_cd)
are expected:

- PAGES_NAME: The full name of the GitHub user pushing the documentation to Pages.
- PAGES_EMAIL: The email address of the GitHub user pushing the documentation to Pages.
- PAGES_PRIVATE_KEY: The private key of the GitHub user pushing the documentation to Pages.
